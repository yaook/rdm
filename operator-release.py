#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import gitlab
from git import Repo
from gitlab.exceptions import GitlabCreateError
from retrying import retry

import os
import datetime


VERSION_FILE_NAME = "version"


def _write_version(version):
    """
    Writes the passed version to the :data:`VERSION_FILE_NAME`.
    """
    with open(f"repo/{VERSION_FILE_NAME}", "w") as f:
        f.write(version)


def _get_current_timestamp():
    """
    Returns the current timestamp in the format to be used by our versions.
    Format is `YYYYMMDD`.
    """
    return int(datetime.datetime.utcnow().strftime("%Y%m%d"))


def _get_target_version(target_branch):
    """
    Generates the version of the currently build release.
    This is specified as below (see https://docs.yaook.cloud/concepts/releases.html#versioning-specification for the original definition)

        In combination with the above we define the following versioning scheme following the SemVer concept (https://semver.org/). stable is hereby using the plain Major.Minor.Patch versioning:
        * We increment Major if we have a incompatible change. This number is defined to be 1.
        * We increment Minor every time we merge rolling into stable. This number is the releasedate in the format “YYYYMMDD”.
        * We increment Patch for every hotfix we merge (see below). It starts from 0 for each normal release.
        For the following examples we define stable as having the version X.Y.Z.
        As rolling will become the next version it will use X.(Y+1).0-rolling.W. Hereby W is the build number being incremented each time during the nightly builds.
        As devel is containing the current development version it is ahead of rolling and using X.(Y+2).0-alpha. The branch uses the always the same version number until rolling pulls the set of changes again.
    """  # noqa E501
    timestamp = _get_current_timestamp()
    if target_branch == "stable":
        return f"0.{timestamp}.0-base"
    elif target_branch == "rolling":
        return f"0.{timestamp+1}.0-rolling.0"
    elif target_branch == "devel":
        return f"0.{timestamp+2}.0-alpha"
    raise ValueError(f"{target_branch} is not supported")


def _create_merge_branch(repo, source_branch, target_branch, next_version):
    """
    Creates the release preparation branch and pushes it.
    We do the following things here:

    1. generate the preparation branch based on the HEAD of the target branch
    2. checkout the preparation branch locally
    3. If a source_branch is specified merge it to the preparation branch
    4. Generate the version file with the new version we have
    5. Commit this change, thereby combining the merge and the version change
    6. push the branch to the origin

    See:
    https://docs.yaook.cloud/concepts/releases.html#operator-implementation
    """

    tmp_branch_name = f"releaseprep/{target_branch}/{next_version}"
    print(f"Building MR in branch {tmp_branch_name}")

    tgt_branch_ref = repo.remotes.origin.refs[target_branch]
    print(f"Hash of target branch {tgt_branch_ref.commit.hexsha}")

    # The following generates a new branch based on the HEAD of target_branch
    # `git branch <tmp_branch_name> <target_branch>`
    repo.create_head(tmp_branch_name, tgt_branch_ref)
    tmp_branch_ref = repo.heads[tmp_branch_name]
    print(f"Hash of tmp branch {tmp_branch_ref.commit.hexsha}")
    parent_commits = (tmp_branch_ref.commit,)

    # Checkout the branch locally. This is required for merge to work.
    # `git checkout <tmp_branch_name> --force`
    tmp_branch_ref.checkout(force=True)

    # We use a source_branch if we want to actually merge new changes.
    # We don't merge a source_branch when we just bump the version devel
    if source_branch:
        if source_branch not in repo.heads:
            repo.create_head(source_branch,
                             repo.remotes.origin.refs[source_branch])
        src_branch_ref = repo.heads[source_branch]
        print(f"Hash of source branch {src_branch_ref.commit.hexsha}")

        # The following does a checkout of the source_branch
        # afterwards we define head as the tmp_branch without resetting
        # the working tree or the index.
        # This also means that we drop all changes in the target_branch.
        # As all hotfixes that might have been made in the target_branch
        # should also be present in the source_branch we should be safe
        # with that.
        # This comes down to having the git tree of the source_branch
        # ready for commit to the tmp_branch
        repo.head.reference = src_branch_ref
        repo.head.reset(index=True, working_tree=True)
        repo.head.reference = tmp_branch_ref

        parent_commits = (tmp_branch_ref.commit, src_branch_ref.commit)

    _write_version(next_version)
    repo.index.add(VERSION_FILE_NAME)

    # The following is then just the missing `git commit` and `git push`
    commit = repo.index.commit(
        f"New {target_branch} release with version {next_version}",
        parent_commits=parent_commits)
    print(f"New commit is {commit.hexsha}")

    repo.remotes.origin.push(
        f"refs/heads/{tmp_branch_name}:refs/heads/{tmp_branch_name}")
    print("Branch pushed")

    return tmp_branch_name


@retry(stop_max_attempt_number=10, wait_fixed=2000)
def _merge_mr(mr):
    """
    This attempts to merge the passed MR in gitlab.
    Since the processing on gitlab side might need some time we
    need to retry a few times.
    """
    try:
        mr.merge(should_remove_source_branch=True,
                 merge_when_pipeline_succeeds=True)
    except gitlab.GitlabOperationError:
        # This happens when the pipeline is already done
        try:
            mr.merge(should_remove_source_branch=True)
        except gitlab.GitlabOperationError as e:
            # This happens if we need an approval for the MR that we cant
            # grant ourselfes. We need user support here
            print(f"WARNING: Requiring user approval for {mr.id}: {e}")


def _create_mr(gl, tmp_branch_name, target_branch, next_version):
    """
    Creates a merge request in gitlab and merges it after the pipeline
    """
    project = gl.projects.get(os.environ["CI_PROJECT_ID"])
    try:
        mr = project.mergerequests.create(
            {'source_branch': tmp_branch_name,
             'target_branch': target_branch,
             'title': f"automatic release for version {next_version}",
             'labels': ['release'],
             'remove_source_branch ': True})
    except GitlabCreateError as e:
        if e.error_message.startswith(
                "Another open merge request already exists"):
            print(f"There seems to be already an MR for {target_branch}")
            mrs = project.mergerequests.list(state='opened')
            mrs = [x for x in mrs if x.source_branch == tmp_branch_name]
            if mrs:
                mr = mrs[0]
                print(f"Attempting to merge MR {mr.id}")
                _merge_mr[mr]
            else:
                print("However we could not fine a merge request. "
                      "Assuming it was merged in the mean time.")
            return
        raise
    else:
        mr.save(remove_source_branch=True)

    print(f"Opened MR {mr.iid}")
    _merge_mr(mr)


def transition(target_branch, repo, gl):
    """
    The main release method. We calculate the target version,
    create the merge branch and merge it in gitlab.
    This implements the transition as specified in
    https://docs.yaook.cloud/concepts/releases.html#versioning-overview
    """
    if target_branch not in ["devel", "rolling", "stable"]:
        raise ValueError(f"unkown target branch: {target_branch}")

    # When source_branch is None we skip merging changes into the
    # target_branch branch later on. This then just bumps the version file
    source_branch = None
    if target_branch == "rolling":
        source_branch = "devel"
    elif target_branch == "stable":
        source_branch = "rolling"
    print(f"Building MR from {source_branch} to {target_branch}")

    next_version = _get_target_version(target_branch)
    print(f"Target version is: {next_version}")

    tmp_branch_name = _create_merge_branch(repo, source_branch, target_branch,
                                           next_version)

    _create_mr(gl, tmp_branch_name, target_branch, next_version)


access_token = os.environ['BOT_GIT_PUSH_OPERATOR_TOKEN']
ci_host = os.environ['CI_SERVER_HOST']
project_path = os.environ['CI_PROJECT_PATH']

repo = Repo.clone_from(
    f"https://gitlab-ci-token:{access_token}@{ci_host}/{project_path}.git",
    "repo",
    branch="devel",
)

gl = gitlab.Gitlab(
    f"https://{ci_host}",
    access_token)
gl.auth()

# Order is important here, as we otherwise might merge from devel
# to rolling and stable in one go
transition("stable", repo, gl)
transition("rolling", repo, gl)
transition("devel", repo, gl)
