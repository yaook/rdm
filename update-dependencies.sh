#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -euox pipefail

# override the path set by the CI, because we're operating on a remote thing
export CI_PROJECT_PATH=yaook/operator
git clone https://"${CI_SERVER_HOST}"/"${CI_PROJECT_PATH}".git operator
pushd operator
git config --global user.name "Yaook CI"
git config --global user.email "yaook.ci@yaook.cloud"

git checkout devel

pip install -e .
for i in $(find yaook/op -mindepth 2 -maxdepth 2 -name __init__.py | cut -d / -f 3); do
    python -m yaook.op "$i" pin-dependencies;
done

git add yaook/assets/pinned_version.yml
git diff --cached

if git diff-index --quiet HEAD --; then
    echo "Nothing changed so we don't need to do anything."
    exit 0;
fi
if git diff-index --quiet "origin/${YAOOK_UPDATE_DEPENDENCIES_BRANCH_NAME}" --; then
    echo "There are changes, but they are already in the target branch, so there should be a MR for them"
    exit 0;
fi

git commit -m "Update dependency pins" # commit the current changes
git checkout -b "${YAOOK_UPDATE_DEPENDENCIES_BRANCH_NAME}"

git remote set-url origin https://gitlab-ci-token:"${BOT_GIT_PUSH_ACCESS_TOKEN}"@"${CI_SERVER_HOST}"/"${CI_PROJECT_PATH}".git
git push --set-upstream origin "${YAOOK_UPDATE_DEPENDENCIES_BRANCH_NAME}" --force -o merge_request.create -o merge_request.label="Needs review" -o merge_request.remove_source_branch # push and automatically create a merge request
