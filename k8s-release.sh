#!/bin/bash

set -euo pipefail

export SOURCE_BRANCH=devel
export CI_PROJECT_PATH=yaook/k8s
export PROJECT_NAME=k8s
export VERSION_FILE=./version

git config --global user.name "${GITLAB_USER_NAME}"
git config --global user.email "${GITLAB_USER_EMAIL}"

git clone https://gitlab-ci-token:"${PUSH_TOKEN}"@"${CI_SERVER_HOST}"/"${CI_PROJECT_PATH}".git "${PROJECT_NAME}"
pushd "${PROJECT_NAME}"

git checkout "${SOURCE_BRANCH}"

if git ls-remote --heads | grep -F "${YAOOK_K8S_RELEASE_PREPARE_BRANCH_PREFIX}"; then
    echo "There is already an open ${YAOOK_K8S_RELEASE_PREPARE_BRANCH_PREFIX}-branch. Was the last release not finished?"
    exit 1;
fi

# get new version-nr from added releasenote files
FIX_RELEASE=False
IFS=. read -ra CURRENT_VERSION < "${VERSION_FILE}"
pushd docs/_releasenotes
if [[ $(find . -maxdepth 1 -iname "*\.breaking*" | wc -l) -gt 0 ]]; then
  VERSION=$(printf '%s.%s.%s' "$((CURRENT_VERSION[0]+1))" "0" "0")
elif [[ $(find . -maxdepth 1 -iname "*\.feature\.*" | wc -l) -gt 0 ]]; then
  VERSION=$(printf '%s.%s.%s' "${CURRENT_VERSION[0]}" "$((CURRENT_VERSION[1]+1))" "0")
else
  VERSION=$(printf '%s.%s.%s' "${CURRENT_VERSION[0]}" "${CURRENT_VERSION[1]}" "$((CURRENT_VERSION[2]+1))")
  FIX_RELEASE=True
fi
popd
echo 'This pipeline is running for version' "${VERSION}"

# get branch-names
YAOOK_K8S_RELEASE_PREPARE_BRANCH=$(printf '%s%s' "${YAOOK_K8S_RELEASE_PREPARE_BRANCH_PREFIX}" "${VERSION}")

## create prepare-branch for release
if [ "${FIX_RELEASE}" = "True" ]; then
  YAOOK_K8S_CURRENT_RELEASE_BRANCH_NAME=$(printf '%s%s' "${YAOOK_K8S_RELEASE_BRANCH_PREFIX}" "${VERSION%.*}")
  echo "name: ${YAOOK_K8S_CURRENT_RELEASE_BRANCH_NAME}"
  # create prepare-branch based on release-branch and merge changes of devel into it
  git checkout -b "${YAOOK_K8S_RELEASE_PREPARE_BRANCH}" origin/"${YAOOK_K8S_CURRENT_RELEASE_BRANCH_NAME}" 
  git merge "${SOURCE_BRANCH}"
else
  git checkout -b "${YAOOK_K8S_RELEASE_PREPARE_BRANCH}" "${SOURCE_BRANCH}"
fi

# write version to file
echo "${VERSION}" > "${VERSION_FILE}"
# generate releasenotes file
towncrier build --version "v${VERSION}" --yes

# commit changes
git add -u
git status

if git diff-index --quiet HEAD --; then
    echo "Nothing changed so we don't need to do anything."
    exit 0;
fi

git commit -m "${YAOOK_K8S_RELEASE_MESSAGE} v${VERSION}"

# push
git remote set-url origin https://gitlab-ci-token:"${PUSH_TOKEN}"@"${CI_SERVER_HOST}"/"${CI_PROJECT_PATH}".git
git push --set-upstream origin "${YAOOK_K8S_RELEASE_PREPARE_BRANCH}"
