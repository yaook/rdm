# Release- and Dependency Management

This repository contains pipeline definitions and helper scripts for the release- and dependency management of Yaook.

They have been factored out of the main repositories to avoid the scheduled release pipelines causing obfuscating the checks of the real code.
